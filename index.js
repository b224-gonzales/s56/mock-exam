function countLetter(letter, sentence) {
    let result = 0
    if (letter.length == 1) {
        for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] == letter) {
        result++;
        }
    }
    return result;
    } else {
    return undefined;
    }
    
    

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const wordLowerCase = text.toLowerCase();

}


function purchase(age, price) {
    if (age < 13) {
        return undefined;
    } else if ((age > 13 && age < 21) || age > 65) {
        let discountedPrice = price - price * 0.2;
        return discountedPrice.toFixed(2).toString();
    } else if (age > 21 && age < 65) {
        return price.toFixed(2).toString();
    }

    
}



function findHotCategories(items) {
    let emptyStocks = [];
    for (let a in items) {
        if (items[a].stocks < 1) {
        emptyStocks.push(items[a].category);
        }
    }
    emptyStocks = emptyStocks.filter(function (item, index, inputArray) {
        return inputArray.indexOf(item) == index;
    });

    return emptyStocks;
}


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let twoVoted = [];
        for (let a in candidateA) {
        for (let b in candidateB) {
            if (candidateA[a] == candidateB[b]) {
            twoVoted.push(candidateA[a]);
            }
        }
    }
    return twoVoted;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};